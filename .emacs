(require 'package)
(add-to-list 'package-archives
  '("melpa" . "http://melpa.milkbox.net/packages/") t)
(add-to-list 'package-archives 
    '("marmalade" .
      "http://marmalade-repo.org/packages/"))
(package-initialize)
(require 'evil nil t)
(evil-mode 1)
(setq evil-auto-indent t)
(when (require 'key-chord nil t)
    (key-chord-mode 1)
    (key-chord-define evil-insert-state-map "jj" 'evil-normal-state))
(smartparens-global-mode t)
(require 'ido)
(ido-mode t)
(load-theme 'zenburn t)
(require 'zencoding-mode)
(zencoding-mode 1)
(require 'auto-indent-mode)
(auto-indent-global-mode)
(require 'yasnippet)
(yas-global-mode t)
(require 'auto-complete-config)
(add-to-list 'ac-dictionary-directories "~/.emacs.d/ac-dict")
(ac-config-default)
(setq ido-enable-flex-matching t)
(setq ido-everywhere t)
(global-auto-revert-mode t)
(setq indent-tabs-mode t)
(setq tab-width 4)
(setq-default tab-width 4)
(setq make-backup-file nil)
(add-hook 'after-init-hook #'global-flycheck-mode)
(setq visible-bell t)
(add-hook 'html-mode-hook
          (lambda()
            (setq sgml-basic-offset 4)
            (setq indent-tabs-mode t)))
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(evil-shift-width 4)
 '(js-auto-indent-flag t)
 '(js-js-switch-tabs t)
 '(make-backup-files nil)
 '(sgml-basic-offset 4))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
